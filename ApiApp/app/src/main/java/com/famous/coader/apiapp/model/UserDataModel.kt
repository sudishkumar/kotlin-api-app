package com.famous.coader.apiapp.model

data class UserDataModel(
    val name:String? = null,
    val email:String? = null,
    val teachers:ArrayList<Teachers>?=null
)
data class Teachers(
    val name:String? = null,
    val age:String? = null
)