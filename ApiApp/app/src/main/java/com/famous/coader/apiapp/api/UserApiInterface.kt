package com.famous.coader.apiapp.api

import com.famous.coader.apiapp.model.UserDataModel
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST


interface UserApiInterface {
    @GET("users")
    fun getUser() : Observable< ArrayList <UserDataModel>> // object in list
//    fun getUser() : Observable<UserDataModel> // for object


    companion object Factory{
        fun  create() : UserApiInterface{
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://demo6546188.mockable.io/")
                .build()

            return  retrofit.create(UserApiInterface::class.java)
        }
    }
}